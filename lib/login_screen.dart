import 'package:flutter/material.dart';
import 'package:project1/counter_screen.dart';

import 'generated/l10n.dart';

const Map account = {'login': 'qwerty', 'password': '123456ab'};
class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();
  final _loginController = TextEditingController();
  final _passController = TextEditingController();
  @override
  void dispose(){
    _loginController.dispose();
    _passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(S.of(context).auth),
      ),
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                const Spacer(),
                Center(
                  child: Text(
                    S.of(context).inputLoginAndPassword,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: S.of(context).login,
                  ),
                  maxLength: 8,
                  controller: _loginController,
                  validator: (login) {
                    if (login == null || login.length < 3) {
                      return S.of(context).inputErrorLoginIsShort;
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: S.of(context).password),
                  maxLength: 16,
                  controller: _passController,
                  obscureText: true,
                  validator: (password) {
                    if (password == null || password.length < 8) {
                      return S.of(context).inputErrorPasswordIsShort;
                    }
                    return null;
                  },
                ),
                const Spacer(),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: () {
                        formKey.currentState!.validate();
                        if (account['login'] == _loginController.text &&
                            account['password'] == _passController.text) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const CounterScreen(),
                            ),
                          );
                        } else {
                          _showMyDialog(context);
                        }
                      },
                      child: Text(S.of(context).signIn)),
                ),
                const SizedBox(
                  height: 20,
                )
              ],
            ),
          )),
    );
  }
}
Future<void> _showMyDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(S.of(context).tryAgain),
        content: SingleChildScrollView(
          child: ListBody(),
        ),
        actions: <Widget>[
          ElevatedButton(
            child: Text(S.of(context).close),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

