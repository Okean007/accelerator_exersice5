import 'package:flutter/material.dart';

class AppLocale{
  List<Locale> localeList = const [Locale('en'),Locale('ru', 'RU')];

  getDefaultLocale(){
    return localeList[1];
  }
  getLocale(String nameLocal){
    switch(nameLocal){
      case 'ru_RU':
        return localeList[1];
      case 'en':
        return localeList[0];
    }
  }
}