
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project1/l10n/app_locale.dart';
import 'generated/l10n.dart';

class CounterScreen extends StatefulWidget {
  const CounterScreen({Key? key}) : super(key: key);

  @override
  State<CounterScreen> createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    final locale = AppLocale();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(S.of(context).homeScreen),
      ),
      body: Container(
        padding: const EdgeInsets.only(bottom: 50),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('${S.of(context).language}: '),
                DropdownButton(
                  value: locale.getLocale(Intl.getCurrentLocale()),
                    items: [
                      DropdownMenuItem(
                          value: locale.getLocale('en'),
                          child: Text(S
                              .of(context)
                              .english)
                      ),
                      DropdownMenuItem(
                          value: locale.getLocale('ru_RU'),
                          child: Text(S
                              .of(context)
                              .russian)
                      ),
                    ],
                    onChanged: <Locale>(value) async {
                      S.load(value);
                      setState(() {});
                    }
                ),
              ],
            ),
            Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                                S.of(context)
                                    .counterValue
                            ),
                            Text(
                              '$_counter',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .headline4,
                            ),
                          ]
                      ),
                    )
                  ],
                )
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: _incrementCounter,
                  child: const Text('+'),
                ),
                ElevatedButton(
                  onPressed: _decrementCounter,
                  child: const Text('-'),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}