import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:project1/l10n/app_locale.dart';
import 'package:project1/screen/screen_person.dart';
import 'generated/l10n.dart';

// import 'package:project1/login_screen.dart';
void main() {
  runApp(const MyApp());
  SystemChrome.setSystemUIOverlayStyle(
     const SystemUiOverlayStyle(
      statusBarColor: Colors.white30,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarIconBrightness: Brightness.light,
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocale();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      locale: locale.getDefaultLocale(),
      home: const PersonScreen(),
    );
  }
}
